/*
Ví dụ 1:
1. Input: tính độ dài cạnh huyền tam giác vuông 3,4

2. Steps:
- step 1: tạo 2 biến chứa 2 cạnh góc vuông
- step 2: tạo biến chứa kết quả cần tìm
- step 3: áp dụng công thức pytago


3. Output: 5
*/
var edge1 = 3;
var edge2 = 4;
var edgeResult = null;
edgeResult = Math.sqrt (edge1 * edge1 + edge2 * edge2); //square root of (edge1 * edge1 + edge2 * edge2)

console.log('Bài ví dụ 1: Cạnh huyền =',edgeResult);

/*
Ví dụ 2:
1. Input: Tìm tổng 3 ký số

2. Steps:
- step 1: cho giá trị 3 ký số là 456
- step 2: tìm giá trị nguyên số hàng trăm của 456 dùng hàm Math.floor(456/100) = 4
- step 3: tìm giá trị nguyên số hàng chục của 456 dùng hàm Math.floor(456%100/10) = số nguyên của 56/10 = 5
- step 4: tìm giá trị nguyên số hàng đơn vị của 456 dùng hàm Math.floor(456%10) = số dư của 56 = 6
- step 5: in ra màn hình log giá trị tổng của step 2, 3, 4

3. Output: 15
*/
var num = 456;
var numHundred = Math.floor(num/100); //Math.floor to make number in integer (no odd)
var numTen = Math.floor(num%100/10);
var numUnit = num%10;
var result = 0;
result = numHundred + numTen + numUnit;

console.log('Bài ví dụ 2: Tổng 3 ký số 456 =', result);
/*
Ví dụ 3:
*/
var tenPhim = 'Zero to Hero at Cybersoft';
var giaVeNguoiLon = 5;
var giaVeTreEm = 3;
var soVeNguoiLon = 1000;
var soVeTreEm = 800;
var phanTram = 10;
var tongDoanhThu = 0;
var tienTuThien = 0;
var tienConLai = 0;
var tongVe = 0;
tongDoanhThu = giaVeNguoiLon * soVeNguoiLon + giaVeTreEm * soVeTreEm;
tienTuThien = tongDoanhThu * phanTram / 100;
tienConLai = tongDoanhThu - tienTuThien;
tongVe = soVeNguoiLon + soVeTreEm;
console.log('Bài ví dụ 3: Tên phim:', tenPhim);
console.log('Bài ví dụ 3: Số vé đã bán:', tongVe);
console.log('Bài ví dụ 3: Doanh thu:', tongDoanhThu,'USD');
console.log('Bài ví dụ 3: Trích % từ thiện:', phanTram,'%');
console.log('Bài ví dụ 3: Tổng tiền trích từ thiện:', tienTuThien,'USD');
console.log('Bài ví dụ 3: Tổng thu được sau khi trừ:', tienConLai,'USD');

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*
Bài tập 1:
1. Input: Tính tiền lương nhân viên làm 24 ngày, 100000/ngày

2. Steps:
- step 1: tạo biến số cho lương 1 ngày, giá trị 100000
- step 2: tạo biến số cho tổng số ngày làm việc, giá trị 24 
- step 3: áp dụng công thức nhân tổng số ngày cho lương 1 ngày
- step 4: in kết quả ra console

3. Output: 2400000
*/
var salaryDay = 100000;
var workDay = 24;
var salaryFinal = 0;
salaryFinal = salaryDay * workDay;
console.log('Bài tập 1: Lương nhận được cho 24 ngày làm =', salaryFinal);

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*
Bài tập 2:
1. Input: Tính giá trị trung bình của 5,10,15,20,25

2. Steps:
- step 1: tạo 5 biến số với giá trị lần lượt 5,10,15,20,25
- step 2: tạo biến tổng = giá trị 5 biến số trên cộng lại
- step 3: tạo biến trung bình = tổng/5
- step 4: in kết quả ra console

3. Output: 15
*/
var num1 = 5;
var num2 = 10;
var num3 = 15;
var num4 = 20;
var num5 = 25;
var sum = 0;
var average = 0;
sum = num1 + num2 + num3 + num4 + num5;
average = sum/5;
console.log('Bài tập 2: Giá trị trung bình của 5,10,15,20,25 =', average);

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*
Bài tập 3: 
1. Input: Quy đổi 5 USD ra tiền VND, tỷ giá USD/VND = 23500

2. Steps:
- step 1: tạo biến số cho tỷ giá USD/VND
- step 2: tạo biến số cho số lượng USD cần đổi
- step 3: tạo biến tiền VND sau khi quy đổi = tỷ giá * số lượng USD
- step 4: in kết quả ra console

3. Output: 
*/
var exchangeRate = 23500;
var numberUsd = 5;
var amountVnd = 0;
amountVnd = exchangeRate * numberUsd;
console.log('Bài tập 3: Số tiền VND đổi ra từ 5 USD =', amountVnd);

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*
Bài tập 4:
1. Input: Tính diện tích & chu vi HCN, dài 10cm, rộng 4cm

2. Steps:
- step 1: tạo biến số cho chiều dài là 10
- step 2: tạo biến số cho chiều rộng là 4
- step 3: tạo biến diện tích = dài * rông
- step 4: tạo biến chu vi = (dài + rộng) * 2
- step 5: in kết quả ra console

3. Output: 40 & 28
*/
var lengthRet = 10;
var widthRet = 4;
var areaRet = 0;
var perimeterRet = 0;
areaRet = lengthRet * widthRet;
perimeterRet = (lengthRet + widthRet) * 2;
console.log('Bài tập 4: Diện tích HCN 10x4 =', areaRet);
console.log('Bài tập 4: Chu vi HCN 10x4 =', perimeterRet);

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*
Bài tập 5:
1. Input: Tính tổng 2 ký số 56

2. Steps:
- step 1: tạo biến số giá trị 2 chữ số 56
- step 2: tạo biến tìm giá trị hàng chục =56/10, dùng Math.floor để lấy số chẵn
- step 3: tạo biến tìm giá trị hàng đơn vị = công thức lấy dư %
- step 4: tạo biến tổng 2 ký số = biến hàng chục + biến hàng đơn vị
- step 5: in kết quả ra console

3. Output: 11
*/
var number = 56;
var numTen = Math.floor(number / 10);
var numUnit = number % 10;
var sumNum = 0;
sumNum = numTen + numUnit;
console.log('Bài tập 5: Tổng của 2 ký số 56 =', sumNum);

